<?php
class lib_fun extends mModel
{
    public function parError($code = 1,$msg = '参数错误',$backdata = array()){
        if (is_array($code)) exit(json_encode($code,JSON_UNESCAPED_UNICODE));
        exit(json_encode(['code'=>$code,'msg'=>$msg,'backdata'=>$backdata],JSON_UNESCAPED_UNICODE));
    }
}