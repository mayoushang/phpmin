<?php
 /*
 * 文件导出导入
 */
if(!file_exists(dirname(__FILE__)."/phar/aliyun-oss-sdk.phar")) file_put_contents(dirname(__FILE__)."/phar/aliyun-oss-sdk.phar", vget("https://cdn.jsdelivr.net/gh/mayoushang/res@1.0/phar/aliyun-oss-sdk.phar"));
//curl get请求
function vget($url){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $tmpInfo = curl_exec($curl);
    if (curl_errno($curl)) {
       return '';
    }
    curl_close($curl);
    return $tmpInfo;
}
require dirname(__FILE__).'/phar/aliyun-oss-sdk.phar';
use OSS\OssClient;
use OSS\Core\OssException;

//阿里云oss上传
class ext_aliyunoss{
    public $config = array(); //配置参数

    public function __construct($config = "") {
        $config = @json_decode($config,true);
        $config['endpoint'] = "oss-cn-qingdao.aliyuncs.com";
        $config = json_encode($config);
        if (!empty($config)) $this->config = json_decode($config);
    }

    //上传到oss
    public function ossUploadFile($filePath){
        if (substr($filePath, 0,1)=="/") $filePath = substr($filePath, 1);
        try {
            $ossClient = new OssClient($this->config->accessKeyId, $this->config->accessKeySecret, $this->config->endpoint);
            $ossClient->uploadFile($this->config->bucket, $filePath, $_SERVER['DOCUMENT_ROOT']."/".$filePath);
        } catch (OssException $e) {
            return "";
        }
        $back_url = "https://".$this->config->bucket.".".$this->config->endpoint."/".$filePath;
        return $back_url;
    }

    //从oss删除
    public function ossDelFile($filePath = array()){
        foreach ($filePath as $key => $one) {
            if (substr($one, 0,1)=="/") $filePath[$key] = substr($one, 1);
        }
        try {
            $ossClient = new OssClient($this->config->accessKeyId, $this->config->accessKeySecret, $this->config->endpoint);
            $ossClient->deleteObjects($this->config->bucket, $filePath);
        } catch (OssException $e) {
            return false;
        }
        return true;
    }

}
?>