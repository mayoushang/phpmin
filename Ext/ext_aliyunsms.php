<?php
/***********************************************
 *		阿里云短信发送第三方扩展类
 * 文件: /include/ext_aliyunsms.php
 * 说明: 自定义第三方扩展类
 * 作者: Myxf
 * 更新: 2015年5月14日
 ***********************************************/

/**
 * 阿里云短信发送第三方扩展类
 */
class ext_aliyunsms {
	//发送阿里云短信
	public function sendSms($RecNum,$ParamString,$TemplateCode,$SignName,$AccessKeyId,$AccessKeySecret){
        $url = 'https://dysmsapi.aliyuncs.com/?';
        $Params['PhoneNumbers'] = $RecNum;
        $Params['SignName'] = $SignName;
        $Params['TemplateCode'] = $TemplateCode;
        $Params['AccessKeyId'] = $AccessKeyId;
        $Params['Action'] = "SendSms";
        $Params['TemplateParam'] = $ParamString;
        $Params['Format'] = 'JSON';
        $Params['RegionId'] = 'cn-hangzhou';
        $Params['SignatureMethod'] = 'HMAC-SHA1';
        $Params['SignatureNonce'] = time();
        $Params['SignatureVersion'] = '1.0'; 
        $Params['Timestamp'] = gmdate("Y-m-d\TH:i:s\Z"); 
        $Params['Version'] = '2017-05-25';
        ksort($Params);
        $Params['Signature'] = $this->computeSignature($Params,$AccessKeySecret);
        $result = $this->vget($url.http_build_query($Params));
        return !isset($result->Code);
    }

    private function computeSignature($parameters, $accessKeySecret)  
    {  
        // 将参数Key按字典顺序排序  
        ksort($parameters);  
        // 生成规范化请求字符串  
        $canonicalizedQueryString = '';  
        foreach($parameters as $key => $value)  
        {  
        $canonicalizedQueryString .= '&' . $this->percentEncode($key)  
            . '=' . $this->percentEncode($value);  
        }  
        // 生成用于计算签名的字符串 stringToSign  
        $stringToSign = 'GET&%2F&' . $this->percentencode(substr($canonicalizedQueryString, 1));  
        //echo "<br>".$stringToSign."<br>";  
        // 计算签名，注意accessKeySecret后面要加上字符'&'  
        $signature = base64_encode(hash_hmac('sha1', $stringToSign, $accessKeySecret . '&', true));  
        return $signature;  
    }

    private function percentEncode($str)  
    {  
        // 使用urlencode编码后，将"+","*","%7E"做替换即满足ECS API规定的编码规范  
        $res = urlencode($str);  
        $res = preg_replace('/\+/', '%20', $res);  
        $res = preg_replace('/\*/', '%2A', $res);  
        $res = preg_replace('/%7E/', '~', $res);  
        return $res;
    }
  
    private function vget($url,$header = array()){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, @$_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $tmpInfo = curl_exec($curl);
        if (curl_errno($curl)) {
           return '';
        }
        curl_close($curl);
        return $tmpInfo;
    }
}
?>