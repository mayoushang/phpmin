<?php
//文件上传
class ext_uploadfile{
    public $max_size = 1024*1024*2;
    public $file_name = 'date';
    public $allow_types;
    public $errmsg = '';//错误信息
    private $ext = '';//上传文件扩展名
    public $save_path;//上传文件保存路径
   

    public function __construct($max_size = 1024*1024*2,$save_path = '/Upload/',$file_name = 'date',$allow_types = '') {
        $this->max_size = $max_size;
        $this->save_path = $save_path;
        $this->file_name   = $file_name;
        $this->allow_types = $allow_types == '' ? 'jpg|gif|png|jpeg|bmp' : $allow_types;
    }

    public function upload_file($files,$uploadpath = '') {
        $name = $files['name'];
        $type = $files['type'];
        $size = $files['size'];
        $tmp_name = $files['tmp_name'];
        $error = $files['error'];

        switch ($error) {
            case 0 : $this->errmsg = '';
                break;
            case 1 : $this->errmsg = '超过了php.ini中文件大小';
                break;
            case 2 : $this->errmsg = '超过了MAX_FILE_SIZE 选项指定的文件大小';
                break;
            case 3 : $this->errmsg = '文件只有部分被上传';
                break;
            case 4 : $this->errmsg = '没有文件被上传';
                break;
            case 5 : $this->errmsg = '上传文件大小为0';
                break;
            default : $this->errmsg = '上传文件失败！';
                break;
            }
        if($error == 0 && is_uploaded_file($tmp_name)) {
            //检测文件大小
            if($size > $this->max_size){
                $this->errmsg = '上传文件太大，最大支持'.ceil($this->max_size/1024/1024).'MB的文件';
                return $this->errmsg;
            }
            $this->ext = $this->get_file_type($name);
            if (empty($this->ext)){
                $this->errmsg = '扩展名不能为空';
                return $this->errmsg;
            }
            $this->save_path = $this->save_path.$uploadpath;
            $this->set_save_path();//设置文件存放路径

            $new_name = $this->file_name != 'date' ? $this->file_name.'.'.$this->ext : date('YmdHis').rand(111111,999999).'.'.$this->ext;//设置新文件名
            //移动文件
            if(move_uploaded_file($tmp_name,$_SERVER['DOCUMENT_ROOT'].$this->save_path.$new_name)){
                $path = $this->save_path.$new_name;
                return $path;
            }else{
                return $this->errmsg;
            }
        }else{
            return $this->errmsg;
        }
    }

    public function check_file_type($filename){
        $ext = $this->get_file_type($filename);
        $this->ext = $ext;
        $allow_types = explode('|',$this->allow_types);
        if(in_array($ext,$allow_types)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function get_file_type($filename){
        $info = pathinfo($filename);
        $ext = empty($info['extension']) ? "" : $info['extension'];
        return strtolower($ext);
    }

    public function set_save_path(){
        $this->save_path = (preg_match('/\/$/',$this->save_path)) ? $this->save_path : $this->save_path . '/';
        if(!is_dir($_SERVER['DOCUMENT_ROOT'].$this->save_path)){
            $this->set_dir($this->save_path);
        }
    }
    public function set_dir($dir = null){
        if(!is_dir($dir)){
            $dir = explode('/', $dir);
            $d = "";
            foreach($dir as $v){
                if($v){
                    $d .= $v . '/';
                    if(!is_dir($d)){
                        mkdir($d, 0777);
                    }
                }
            }
        }
        return true;
    }  
}
?>