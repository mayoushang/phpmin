<?php
if(!defined('APP_VER')) exit("die!");

class main extends App
{	
	//示例入口
	public function index(){
		$this->text = "Hello Word"; //模板赋值
		$this->display("index.html"); //输出到模板
	}

	//第三方扩展
	public function vendor(){
		$requestUtils = new RequestUtils(); //composer引入的直接new
	}

	//调用Class,Ext
	public function classs(){
		$http = $this->mClass("ext_http");
		echo $http->vget("https://www.baidu.com/");
	}

	//调用Model
	public function model(){
		$lib_api = $this->mModel("lib_api",['123','456']);
		echo $lib_api->test();
	}

	//接收数据
	public function args(){
		echo $this->mArgs("id");
	}

	//文件缓存
	public function accesss(){
		$this->access("test","123");
		echo $this->access("test");
	}
}
?>