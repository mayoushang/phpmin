<?php
class lib_redis extends mModel
{
    var $ip = "127.0.0.1";
    var $mkey = "";
    var $port = "6378";
    var $redis = null;

    function __construct(){
        if (!class_exists('Redis')){
          exit("未安装Redis扩展！");
        }
        $this->start();
    }

    function start(){
        $redis = new Redis();
        $redis->connect($this->ip, $this->port);
        $redis->auth($this->mkey);
        $redis->select(0);
        $this->redis = $redis;
    }

    /**
     * 获取一条数据
     * @access public
     * @param string $keys key值
     * @return string $ret 返回value
     */
    function getOne($key){
        $ret = $this->redis->get($key);
        return $ret;
    }

    /**
     * 获取多条数据
     * @access public
     * @param string $keys key值，参数为*时返回全部也可key*，也可以使用,分割返回多个，例如key1,key2
     * @return array $ret 返回value
     */
    function getAll($keys = "*"){
      $_ret = array();
      if (strpos($keys, "*")!==false){
        $ret = $this->redis->keys($keys);
        foreach ($ret as $key => $one) {
          $_ret[$key]['key'] = $one;
          $_ret[$key]['value'] = $this->getOne($one);
        }
        $ret = $_ret;
      }else{
        $keys = explode(",", $keys);
        $ret = $this->redis->mget($keys);
        foreach ($ret as $key => $one) {
          $_ret[$key]['key'] = $keys[$key];
          $_ret[$key]['value'] = $one;
        }
        $ret = $_ret;
      }
        return $ret;
    }

    /**
     * 添加一条数据
     * @access public
     * @param string $keys key
     * @param string $value 值
     */
    function add($key,$value){
        $this->redis->set($key, $value);
    }

    /**
     * 修改一条数据
     * @access public
     * @param string $keys key
     * @param string $value 值
     * @param boolean $ret 返回结果
     */
    function edit($key,$value,$expire = 0,$expire_val = 1){
        $ttl = intval($this->redis->ttl($key));
        if (!empty($expire)) $ttl = $expire;
        $ret = $this->redis->getSet($key, $value);
        if($ttl>0){
            $this->expire($key,$ttl);
        }else{
            $this->expire($key,$expire_val);
        }
        return $ret;
    }

    /**
     * 删除一条数据
     * @access public
     * @param string $keys key，默认*删除全面或key*，可以指定一条例如key1
     * @param boolean $ret 返回结果
     */
    function del($key = "*"){
        $ret = $this->redis->del($key);
        return $ret;
    }

    /**
     * key是否存在
     * @access public
     * @param string $keys key
     * @param boolean $ret 返回结果
     */
    function exists($key){
        $ret = $this->redis->exists($key);
        return $ret;
    }

     /**
     * 设置存活时间，到期自动删除
     * @access public
     * @param string $keys key
     * @param string $time 过期时间，单位秒
     * @param boolean $ret 返回结果
     */
    function expire($key,$time){
        $ret = $this->redis->expire($key,$time);
        return $ret;
    }

    function ckLimit($key,$limit){
        $key = "ckLimit_".$key;
        if ($this->redis->ttl($key)<0){
            $this->add($key,0);
            $this->expire($key,3600);
        }
        $_limit = $this->getOne($key);
        $_limit = $_limit + 1;
        $this->edit($key,$_limit);
        return ($limit - $_limit);
    }
}