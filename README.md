# phpMVC微型框架phpmin，不到50KB，麻雀虽小五脏俱全

#### 介绍
phpMVC微型框架phpmin，不到50KB，麻雀虽小五脏俱全

#### 软件架构
Controller：控制器
Ext：扩展类型，app_开头的为系统扩展不可删除，其他随意
	kindeditor：富文本编辑器，可删除
	phar：phar引用文件，不可删除
	app_mysqli.php：数据库操作，不可删除
	app_smarty.php：模板引擎，不可删除
Model：数据操作
	lib_api.php：示例新建类
	lib_db.php：mysql操作
	lib_redis.php：redis操作
Tmp：缓存文件夹
upload：文件上传文件夹
vendor：第三方扩展类
View：模板目录
app.php：框架类
config.php：配置文件
index.php：框架入口

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
